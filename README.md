**Language Used**: 100% Kotlin

**Dependencies**: 
- RxJava2
- RxAndroid
- Room DB
- Lifecycle Extenstions (ViewModel & LiveData)
- Retrofit2
- Gson 
- Picasso
- Timber
- Koin

**Architectural Pattern**: MVVM + Repository

**Main Features**
- Displays a list of Comics from Marvel Comic API. For each comic, thumbnail and title are shown
- Endless Scrolling behavior to autoload Comics
- Supports Caching Data