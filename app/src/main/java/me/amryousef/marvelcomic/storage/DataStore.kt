package me.amryousef.marvelcomic.storage

import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.functions.Function4
import io.reactivex.schedulers.Schedulers
import me.amryousef.marvelcomic.model.ComicList
import me.amryousef.marvelcomic.network.ApiService
import timber.log.Timber

class DataStore(private val apiService:ApiService,private val db:PersistenceDatabase): IDataStore {
    override fun getCachedData(): Flowable<List<ComicList>> {
        return Flowable.zip(
                db.dao().getAllComicLists(),
                db.dao().getAllComics(),
                db.dao().getAllCharacters(),
                db.dao().getAllCreators(),
                Function4 { comicLists, comics, characters, creators ->
                    comics.forEach {
                        comic ->
                        comic.charactersList = characters.filter { character -> character.comicId == comic.comicId }
                        comic.creatorsList = creators.filter { creator -> creator.comicId == comic.comicId }
                    }
                    comicLists.forEach {
                        list ->
                        list.comics = comics.filter { comic -> comic.comicListId == list.listId }
                    }

                    return@Function4 comicLists
                }
        )
    }

    override fun getMoreRemoteData(onError:(Throwable) -> Unit){
        db.dao().getAllComicLists()
                .firstElement()
                .flatMap {
                    val offset = it.size * 20
                    apiService.fetchComicsList(20,offset).firstElement()
                }.flatMap {
                    Maybe.fromCallable {
                        val comicListId = db.dao().insertComicLists(listOf(it))[0]
                        if (it.comics != null) {
                            it.comics?.forEach {
                                it.comicListId = comicListId
                            }
                            val comicListIds = db.dao().insertComics(it.comics!!)
                            it.comics?.forEachIndexed { index, comic ->
                                comic.charactersList?.forEach { it.comicId = comicListIds[index] }
                                db.dao().insertCharacter(comic.charactersList!!)
                                comic.creatorsList?.forEach { it.comicId = comicListIds[index] }
                                db.dao().insertCreator(comic.creatorsList!!)
                            }
                        }
                    }
                }
                .subscribeOn(Schedulers.io())
                .subscribe(
                        {

                        },
                        {
                            onError(it)
                            Timber.e(it)
                        }
                )

    }
}