package me.amryousef.marvelcomic.storage

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import me.amryousef.marvelcomic.model.Character
import me.amryousef.marvelcomic.model.Comic
import me.amryousef.marvelcomic.model.ComicList
import me.amryousef.marvelcomic.model.Creator

@Database(
        entities = [ComicList::class,Comic::class,Creator::class,Character::class],
        version = 1,
        exportSchema = false
)
abstract class PersistenceDatabase: RoomDatabase() {
    abstract fun dao(): Dao
    companion object {
        private var instance:PersistenceDatabase ? = null
        fun getDatabase(context: Context): PersistenceDatabase {
            if (instance == null)
                instance = Room.databaseBuilder(context,PersistenceDatabase::class.java,"marvel_comic_db").build()
            return instance!!
        }
    }
}