package me.amryousef.marvelcomic.storage

import io.reactivex.Flowable
import me.amryousef.marvelcomic.model.ComicList

interface IDataStore {
    fun getCachedData(): Flowable<List<ComicList>>
    fun getMoreRemoteData(onError:(Throwable) -> Unit)
}