package me.amryousef.marvelcomic.storage

import android.arch.persistence.room.*
import android.arch.persistence.room.Dao
import io.reactivex.Flowable
import me.amryousef.marvelcomic.model.Character
import me.amryousef.marvelcomic.model.Comic
import me.amryousef.marvelcomic.model.ComicList
import me.amryousef.marvelcomic.model.Creator

@Dao
interface Dao {
    @Query("SELECT * FROM ComicList")
    fun getAllComicLists():Flowable<List<ComicList>>

    @Query("SELECT * FROM Comic WHERE comicListId = :id")
    fun getComicsForList(id: Long):Flowable<List<Comic>>

    @Query("SELECT * FROM Comic")
    fun getAllComics():Flowable<List<Comic>>

    @Query("SELECT * FROM Creator WHERE comicId = :id")
    fun getCreatorsForComic(id:Long):Flowable<List<Creator>>

    @Query("SELECT * FROM Creator")
    fun getAllCreators():Flowable<List<Creator>>

    @Query("SELECT * FROM Character WHERE comicId = :id")
    fun getCharactersForComic(id:Long):Flowable<List<Character>>

    @Query("SELECT * FROM Character")
    fun getAllCharacters():Flowable<List<Character>>

    @Insert(onConflict =  OnConflictStrategy.REPLACE)
    fun insertComicLists(lists:List<ComicList>): List<Long>

    @Insert(onConflict =  OnConflictStrategy.REPLACE)
    fun insertComics(lists:List<Comic>): List<Long>

    @Insert(onConflict =  OnConflictStrategy.REPLACE)
    fun insertCreator(lists:List<Creator>)

    @Insert(onConflict =  OnConflictStrategy.REPLACE)
    fun insertCharacter(lists:List<Character>)

    @Query("DELETE FROM ComicList")
    fun clearComicListTable()
}