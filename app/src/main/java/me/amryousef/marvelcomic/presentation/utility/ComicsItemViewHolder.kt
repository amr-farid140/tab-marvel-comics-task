package me.amryousef.marvelcomic.presentation.utility

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.comics_list_item.view.*
import me.amryousef.marvelcomic.R
import me.amryousef.marvelcomic.di.setCustomTypeFace
import me.amryousef.marvelcomic.model.Comic

/**
 * View Holder for Comics List Item.
 */
class ComicsItemViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    private val thumbnail = itemView.comics_list_item_thumbnail as ImageView
    val title = itemView.comics_list_item_title as TextView

    /**
     * Initialize view according to the comic entry data
     * @param comic - Comic Entry
     */
    fun bindComic(comic:Comic) {
        title.text = comic.title.toString()
        title.setCustomTypeFace(itemView.context)
        if (comic.thumbnailUrl != null && !(comic.thumbnailUrl!!.endsWith("image_not_available")||comic.thumbnailUrl!!.contains("image_not_available"))) {
            Picasso
                    .get()
                    .load("${comic.thumbnailUrl}/landscape_incredible.jpg")
                    .placeholder(R.drawable.image_placeholder)
                    .error(R.drawable.image_placeholder)
                    .into(thumbnail)
        }
    }
}