package me.amryousef.marvelcomic.presentation.utility

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import me.amryousef.marvelcomic.R
import me.amryousef.marvelcomic.model.Comic

/**
 * Adapter for Comic List RecyclerView.
 * Uses ComicsItemViewHolder as View holder
 * @param comics List of comics to display
 */
class ComicsListAdapter(var comics:List<Comic>): RecyclerView.Adapter<ComicsItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComicsItemViewHolder {
        return ComicsItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.comics_list_item,parent,false))
    }

    override fun getItemCount(): Int {
        return comics.size
    }

    override fun onBindViewHolder(holder: ComicsItemViewHolder, position: Int) {
        holder.bindComic(comics[position])
    }

}