package me.amryousef.marvelcomic.presentation.utility

import android.support.v7.util.DiffUtil
import me.amryousef.marvelcomic.model.Comic

/**
 * DiffUtil Callback Implementation to calculate difference between to lists
 * @param current Currently displayed list of comics
 * @param next List of comics to be displayed next
 */
class ComicsListDiffUtil(private val current: List<Comic>, private val next: List<Comic>): DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return current[oldItemPosition].comicId == next[newItemPosition].comicId
    }

    override fun getOldListSize(): Int {
        return current.size
    }

    override fun getNewListSize(): Int {
        return next.size
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return current[oldItemPosition] == next[newItemPosition]
    }

}