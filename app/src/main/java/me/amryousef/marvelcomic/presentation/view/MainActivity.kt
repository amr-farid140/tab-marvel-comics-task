package me.amryousef.marvelcomic.presentation.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import me.amryousef.marvelcomic.R

/**
 * Main Entry for the application
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
                .replace(R.id.main_activity_fragment_holder,ComicsListFragment(),ComicsListFragment.tag)
                .commitAllowingStateLoss()
    }
}
