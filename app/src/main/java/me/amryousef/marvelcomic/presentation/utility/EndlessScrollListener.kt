package me.amryousef.marvelcomic.presentation.utility

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.support.v7.widget.LinearLayoutManager


/**
 * Implementation for RecyclerView.OnScrollListener which includes an abstract function which is called when
 * scrolling is at a set visibleItemsThreshold.
 * @param layoutManager Recycler view layout manager
 * @param visibleItemsThreshold Threshold of visible items required to call the abstract function
 */
abstract class EndlessScrollListener(private val layoutManager: GridLayoutManager,private var visibleItemsThreshold: Int): RecyclerView.OnScrollListener() {
    private var currentPage = 0
    private var previousItemsCount = 0
    private var startPageIndex = 0
    var loading = false
    init {
        visibleItemsThreshold *= layoutManager.spanCount
    }
    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        var lastVisibleItemPosition = 0
        val totalItemCount = layoutManager.itemCount
        lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()

        if (totalItemCount < previousItemsCount) {
            this.currentPage = this.startPageIndex
            this.previousItemsCount = totalItemCount
            if (totalItemCount == 0) {
                this.loading = true
            }
        }

        if (loading && totalItemCount > previousItemsCount) {
            loading = false
            previousItemsCount = totalItemCount
        }

        if (!loading && lastVisibleItemPosition + visibleItemsThreshold > totalItemCount) {
            currentPage++
            onLoadMore()
            loading = true
        }
    }

    abstract fun onLoadMore()
}