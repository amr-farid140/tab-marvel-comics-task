package me.amryousef.marvelcomic.presentation.viewModel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import me.amryousef.marvelcomic.model.Comic
import me.amryousef.marvelcomic.model.ComicList
import me.amryousef.marvelcomic.storage.IDataStore
import timber.log.Timber

/**
 * View model for comics list.
 * Handles reading data from the datastore and requesting more data to be loaded
 *
 * @param dataStore Datastore instance
 */
class ComicsListViewModel(private val dataStore: IDataStore): ViewModel() {

    private val mutableComicLists: MutableLiveData<List<Comic>> = MutableLiveData()
    val comicsLiveData:LiveData<List<Comic>> get() = mutableComicLists

    private var dataAccessDisposable: Disposable? = null
    var onError : (Throwable) -> Unit = {}

    init {
        dataAccessDisposable = dataStore.getCachedData()
                .subscribeOn(Schedulers.io())
                .subscribe(
                        {
                            if (!it.isEmpty()) {
                                updateComicsValue(it)
                            } else {
                                onLoadMoreRequested()
                            }
                        },
                        {
                            Timber.e(it)
                            onError(it)
                        }
                )
    }

    /**
     * Updates the value of comics list live data
     * @param comicLists Received ComicLists
     */
    private fun updateComicsValue(comicLists:List<ComicList>) {
        val comics = ArrayList<Comic>()
        comicLists.forEach {
            if (it.comics != null)
                comics.addAll(it.comics!!)
        }
        this.mutableComicLists.postValue(comics)
    }

    /**
     * Request more data from datastore
     */
    fun onLoadMoreRequested() {
        dataStore.getMoreRemoteData(onError)
    }

    override fun onCleared() {
        dataAccessDisposable?.dispose()
        super.onCleared()
    }
}