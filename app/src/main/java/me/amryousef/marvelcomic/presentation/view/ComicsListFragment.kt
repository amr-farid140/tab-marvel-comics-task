package me.amryousef.marvelcomic.presentation.view


import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_comics_list.view.*
import me.amryousef.marvelcomic.R
import me.amryousef.marvelcomic.di.setCustomTypeFace
import me.amryousef.marvelcomic.presentation.utility.ComicsListAdapter
import me.amryousef.marvelcomic.presentation.utility.ComicsListDiffUtil
import me.amryousef.marvelcomic.presentation.utility.EndlessScrollListener
import me.amryousef.marvelcomic.presentation.viewModel.ComicsListViewModel
import org.koin.android.architecture.ext.viewModel


/**
 * Fragment to Display list of Comics
 * Uses a ViewModel of type ComicsListViewModel
 */
class ComicsListFragment : Fragment() {

    private val viewModel by viewModel<ComicsListViewModel>()
    private val listAdapter: ComicsListAdapter = ComicsListAdapter(ArrayList())
    private var scrollListener: EndlessScrollListener ? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        view?.fragment_comics_toolbar_title?.setCustomTypeFace(requireActivity())
        return inflater.inflate(R.layout.fragment_comics_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewModel.onError = {
            view.fragment_comics_list_refresh_layout.isRefreshing = false
            activity?.runOnUiThread {
                Toast.makeText(context,"Failed to load data",Toast.LENGTH_SHORT).show()
            }
        }
        view.fragment_comics_list.itemAnimator = DefaultItemAnimator()
        view.fragment_comics_list.adapter = listAdapter
        view.fragment_comics_list_refresh_layout.setOnRefreshListener {
            viewModel.onLoadMoreRequested()
        }
        scrollListener = object : EndlessScrollListener(view.fragment_comics_list.layoutManager as GridLayoutManager,5) {
            override fun onLoadMore() {
                view.fragment_comics_list_refresh_layout.isRefreshing = true
                viewModel.onLoadMoreRequested()
            }

        }
        view.fragment_comics_list.addOnScrollListener(scrollListener)
        viewModel.comicsLiveData.observe(this, Observer {
            if (it != null) {
                val diffResult = DiffUtil.calculateDiff(ComicsListDiffUtil(listAdapter.comics, it))
                listAdapter.comics = it
                diffResult.dispatchUpdatesTo(listAdapter)
                view.fragment_comics_list_refresh_layout.isRefreshing = false
                scrollListener?.loading = false
            }
        })

    }

    override fun onDestroy() {
        if (scrollListener != null)
            view?.fragment_comics_list?.removeOnScrollListener(scrollListener)
        super.onDestroy()
    }


    companion object {
        const val tag = "ComicsListFragment"
    }
}
