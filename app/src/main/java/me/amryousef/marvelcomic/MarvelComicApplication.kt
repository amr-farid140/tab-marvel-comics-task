package me.amryousef.marvelcomic

import android.app.Application
import me.amryousef.marvelcomic.di.KoinModules
import me.amryousef.marvelcomic.storage.PersistenceDatabase
import timber.log.Timber

class MarvelComicApplication: Application() {
    override fun onCreate() {
        KoinModules.build(this)
        Timber.plant(Timber.DebugTree())
        super.onCreate()
    }
}