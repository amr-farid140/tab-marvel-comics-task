package me.amryousef.marvelcomic.di

import android.app.Application
import com.google.gson.Gson
import me.amryousef.marvelcomic.R
import me.amryousef.marvelcomic.model.ComicList
import me.amryousef.marvelcomic.model.utility.ComicDeserializer
import me.amryousef.marvelcomic.network.ApiService
import me.amryousef.marvelcomic.network.RequestInterceptor
import me.amryousef.marvelcomic.presentation.viewModel.ComicsListViewModel
import me.amryousef.marvelcomic.storage.DataStore
import me.amryousef.marvelcomic.storage.IDataStore
import me.amryousef.marvelcomic.storage.PersistenceDatabase
import okhttp3.OkHttpClient
import org.koin.android.ext.android.startKoin
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Container for all the Koin module required for dependency injection
 * https://github.com/Ekito/koin
 * Koin simplifies dependency injection and works well with kotlin when compared with Dagger.
 *
 * @param context Application Context required for reading values from resources
 */
class KoinModules private constructor(private val context: Application) {

    // Creates an HttpClient instance with a request interceptor set-up
    private val okHttpClient: OkHttpClient get() {
        return OkHttpClient()
                .newBuilder()
                .addInterceptor(
                        RequestInterceptor(
                                context.getString(R.string.api_public_key),
                                context.getString(R.string.api_private_key)
                        )
                )
                .build()
    }

    // Initializes a Gson instance with custom type adapter
    private val gson:Gson get() {
        return Gson().newBuilder().registerTypeAdapter(ComicList::class.java,ComicDeserializer()).create()
    }

    // Instance of Retrofit ApiService.
    // Initialized with the custom HttpClient, Gson Converter and RxJava adapter
    private val apiService:ApiService get() {
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(context.getString(R.string.baseUrl))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build().create(ApiService::class.java)
    }


    private val gsonModule: Module = applicationContext {
        bean { gson }
    }
    private val apiServiceModule: Module = applicationContext {
        bean { apiService }
    }
    private val dataStoreModule: Module = applicationContext {
        bean { DataStore(get(),get()) as IDataStore }
        bean { apiService }
        bean { PersistenceDatabase.getDatabase(androidApplication()) }
    }
    private val comicsListModelModule = applicationContext {
        bean { ComicsListViewModel(get()) }
    }
    init {
        context.startKoin(listOf(gsonModule,apiServiceModule,dataStoreModule,comicsListModelModule))
    }

    companion object {
        /**
         * Builder function for Koin modules. Initializes and instance
         * of KoinModules class which calls startKoin()
         * @param context Application Context required for reading values from resources
         */
        fun build(context: Application){
            KoinModules(context)
        }
    }
}