package me.amryousef.marvelcomic.di

import android.content.Context
import android.graphics.Typeface
import android.widget.TextView

/**
 * An Extension for TextView to set a custom typeface from a file located in the assets folder
 */
fun TextView.setCustomTypeFace(context: Context) {
    val typeface = Typeface.createFromAsset(context.assets,"BentonSans-Comp-Black-Regular.ttf")
    this.typeface = typeface
}