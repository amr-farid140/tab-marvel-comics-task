package me.amryousef.marvelcomic.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import com.google.gson.annotations.Expose

@Entity(
        indices = [(Index(value = ["listId"],name = "listId"))]
)
data class ComicList(
        val offset: Int,
        val limit: Int,
        val total: Int,
        val count: Int) {
    @PrimaryKey(autoGenerate = true)
    var listId:Long? = null
    @Ignore
    var comics: List<Comic> ? = null
    var attributionText: String? = null
    var attributionHtml: String? = null

}