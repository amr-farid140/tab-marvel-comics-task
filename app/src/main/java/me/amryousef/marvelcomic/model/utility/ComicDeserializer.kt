package me.amryousef.marvelcomic.model.utility

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import me.amryousef.marvelcomic.model.Comic
import me.amryousef.marvelcomic.model.ComicList
import me.amryousef.marvelcomic.model.Creator
import java.lang.reflect.Type

class ComicDeserializer: JsonDeserializer<ComicList> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): ComicList {
        if (json != null) {
            if (json.isJsonObject) {
                val jObject = json.asJsonObject
                if (jObject.has("data")) {
                    val attributionText = jObject.get("attributionText").asString
                    val attributionHtml = jObject.get("attributionHTML").asString
                    val comicListType = object : TypeToken<List<Comic>>() {}.type
                    val comics =
                            Gson()
                                    .newBuilder()
                                    .registerTypeAdapter(Comic::class.java,CreatorListDeserializer())
                                    .create()
                                    .fromJson<List<Comic>>(jObject.getAsJsonObject("data").getAsJsonArray("results"),comicListType)
                    val comicList = Gson().fromJson<ComicList>(jObject.getAsJsonObject("data"),ComicList::class.java)
                    comicList.comics = comics
                    comicList.attributionHtml = attributionHtml
                    comicList.attributionText = attributionText
                    return comicList

                } else {
                    throw Throwable("key=data not found")
                }

            } else {
                throw Throwable("Invalid JSON Received. Expected JSON Object.")
            }
        } else {
            throw Throwable("Null JSON Received")
        }
    }

}