package me.amryousef.marvelcomic.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey

@Entity(
        indices = [(Index(value = ["creatorId","comicId"]))],
        foreignKeys = [(ForeignKey(entity = Comic::class,parentColumns = ["comicId"],childColumns = ["comicId"],onUpdate = ForeignKey.CASCADE,onDelete = ForeignKey.CASCADE))]
)
data class Creator(
        val name: String?,
        val role: String?,
        val resourceURI: String?
) {
    @PrimaryKey(autoGenerate = true)
    var creatorId: Long = 0L
    var comicId: Long = 0L

}