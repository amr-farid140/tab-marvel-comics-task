package me.amryousef.marvelcomic.model.utility

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.reflect.TypeToken
import me.amryousef.marvelcomic.model.Character
import me.amryousef.marvelcomic.model.Comic
import me.amryousef.marvelcomic.model.Creator
import java.lang.reflect.Type

class CreatorListDeserializer: JsonDeserializer<Comic> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Comic {

        if (json != null && json.isJsonObject) {
            val gson = Gson()
            val comic = gson.fromJson<Comic>(json,Comic::class.java)
            val creatorListType = object : TypeToken<List<Creator>>() {}.type
            val characterListType = object : TypeToken<List<Character>>() {}.type

            val jObject = json.asJsonObject
            if (jObject.has("creators")) {
                val items = jObject.getAsJsonObject("creators").getAsJsonArray("items")
                comic.creatorsList = gson.fromJson(items,creatorListType)
            }
            if (jObject.has("characters")) {
                val items = jObject.getAsJsonObject("characters").getAsJsonArray("items")
                comic.charactersList = gson.fromJson(items,characterListType)
            }
            if (jObject.has("thumbnail")) {
                comic.thumbnailUrl = jObject.getAsJsonObject("thumbnail").get("path").asString
            }
            return comic
        } else {
            throw Throwable("Invalid JSON Received")
        }
    }

}