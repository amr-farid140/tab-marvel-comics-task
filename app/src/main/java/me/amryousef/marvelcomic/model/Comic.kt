package me.amryousef.marvelcomic.model

import android.arch.persistence.room.*

@Entity(
        indices = [(Index(value = ["comicId","comicListId"]))],
        foreignKeys = [(ForeignKey(entity = ComicList::class,parentColumns = ["listId"],childColumns = ["comicListId"],onDelete = ForeignKey.CASCADE,onUpdate = ForeignKey.CASCADE))]
)
data class Comic(
        val title: String?,
        val description: String?,
        val issueNumber: Double?
) {
    @PrimaryKey
    var comicId: Long ?  = null
    var comicListId: Long = 0L
    @Ignore
    var charactersList: List<Character> ? = null
    @Ignore
    var creatorsList: List<Creator> ? = null
    var thumbnailUrl: String? = null
}