package me.amryousef.marvelcomic.network

import okhttp3.Interceptor
import okhttp3.MediaType
import okhttp3.Response
import okhttp3.ResponseBody
import java.security.MessageDigest
import java.security.Timestamp
import java.util.*

/**
 * Request Interceptor which is responsible for injecting request authentication
 * parameters required by the API provider
 */
class RequestInterceptor(private val publicKey: String, private val privateKey: String): Interceptor {

    override fun intercept(chain: Interceptor.Chain?): Response {
        return if (chain != null) {
            var request = chain.request()
            var httpUrl = request.url()
            val builder = httpUrl.newBuilder()
            val timestamp = Date().time.toString()
            builder.addQueryParameter("ts",timestamp)
            builder.addQueryParameter("hash",generateHash(timestamp))
            builder.addQueryParameter("apikey",publicKey)
            httpUrl = builder.build()
            request = request.newBuilder().url(httpUrl).build()
            chain.proceed(request)
        } else{
            Response.Builder().body(ResponseBody.create(MediaType.parse("text/plain"),"Invalid Chain Received. Failed to initialize interceptor.")).build()
        }
    }

    /**
     * Generates MD5 hash based on MD5(timestamp + private key + public key)
     * @return String representation of MD5 hash
     */
    private fun generateHash(timestamp: String): String {
        val hexChars = "0123456789abcdef"
        val bytes = MessageDigest
                .getInstance("MD5")
                .digest("$timestamp$privateKey$publicKey".toByteArray())
        val result = StringBuilder(bytes.size * 2)

        bytes.forEach {
            val i = it.toInt()
            result.append(hexChars[i shr 4 and 0x0f])
            result.append(hexChars[i and 0x0f])
        }

        return result.toString()
    }

}