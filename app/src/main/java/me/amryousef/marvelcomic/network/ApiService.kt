package me.amryousef.marvelcomic.network

import io.reactivex.Observable
import me.amryousef.marvelcomic.model.ComicList
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("v1/public/comics")
    fun fetchComicsList(@Query("limit") limit: Int, @Query("offset") offset: Int): Observable<ComicList>
}