package me.amryousef.marvelcomic

import com.google.gson.Gson
import me.amryousef.marvelcomic.model.ComicList
import me.amryousef.marvelcomic.model.utility.ComicDeserializer
import me.amryousef.marvelcomic.network.ApiService
import me.amryousef.marvelcomic.network.RequestInterceptor
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Tests the API Service requests are successfully intercepted and authentication parameters are added
 * as well as successful deserialization of JSON to objects
 */
@RunWith(JUnit4::class)
class RemoteDataRetrievalTest {
    private lateinit var  apiService: ApiService
    private lateinit var mockWebServer: MockWebServer

    private val okHttpClient: OkHttpClient
        get() {
            return OkHttpClient()
                    .newBuilder()
                    .connectTimeout(200, TimeUnit.SECONDS)
                    .writeTimeout(200, TimeUnit.SECONDS)
                    .readTimeout(200, TimeUnit.SECONDS)
                    .addInterceptor(
                            RequestInterceptor(
                                    "123",
                                    "1234"
                            )
                    )
                    .build()
        }
    private val gson: Gson
        get() {
            return Gson().newBuilder().registerTypeAdapter(ComicList::class.java, ComicDeserializer()).create()
        }

    @Before
    fun setup() {
        mockWebServer = MockWebServer()
        val url = mockWebServer.url("/")
        apiService = Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build().create(ApiService::class.java)
    }

    /**
     * Verify that request is intercepted and authentication parameters added
     */
    @Test
    fun requestIsIntercepted() {
        mockWebServer.enqueueValidResponse()
        apiService.fetchComicsList(20,0)
                .test().assertValue {
                    val request = mockWebServer.takeRequest()
                    val pathParts = request.path.split("?")
                    if (pathParts.size == 2 ) {
                        pathParts[0] == "/v1/public/comics" &&
                                pathParts[1].contains("ts") &&
                                pathParts[1].contains("hash") &&
                                pathParts[1].contains("apikey")
                    } else {
                        false
                    }
                }
    }

    /**
     * Verify JSON is parsed Successfully
     */
    @Test
    fun successfulDeserialization() {
        mockWebServer.enqueueValidResponse()
        apiService.fetchComicsList(20,0)
                .test()
                .assertValue {
                    comicList ->
                    comicList.comics != null && comicList.comics!!.size == 20 && comicList.comics!!.map { !it.title.isNullOrEmpty() }.reduce { acc, b -> acc && b }
                }
    }

    @After
    fun teardown(){
        mockWebServer.shutdown()
    }
}