package me.amryousef.marvelcomic

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.support.test.runner.AndroidJUnit4
import junit.framework.Assert
import me.amryousef.marvelcomic.presentation.viewModel.ComicsListViewModel
import me.amryousef.marvelcomic.storage.DataStore
import me.amryousef.marvelcomic.utility.TestApplication
import me.amryousef.marvelcomic.utility.enqueueValidResponse
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.standalone.inject
import org.koin.test.KoinTest
import java.util.concurrent.CountDownLatch

/**
 * Test Comics List View model to verify it handles data updates
 * and able to refresh data when load more is requested
 */
@RunWith(AndroidJUnit4::class)
class ComicsListViewModelTest: KoinTest {
    @get:Rule val instantExecuteRule = InstantTaskExecutorRule()
    private val dataStore: DataStore by inject()
    private lateinit var viewModel: ComicsListViewModel

    /**
     * Setup database with initial values and initialize view model
     */
    @Before
    fun setup() {
        TestApplication.mockWebServer!!.enqueueValidResponse()
        viewModel = ComicsListViewModel(dataStore)
        viewModel.onError = {
            throw  it
        }
    }

    /**
     * Verify that live data is emitting updates
     */
    @Test
    fun testLiveDataUpdated() {
        val countDownLatch = CountDownLatch(1)
        viewModel.comicsLiveData.observeForever {
            Assert.assertNotNull(it)
            countDownLatch.countDown()
        }
        countDownLatch.await()
    }

    /**
     * Verify that live data is updated with new path of comics whenever load more is requested
     */
    @Test
    fun testCanLoadMore() {
        val countDownLatch = CountDownLatch(1)
        viewModel.comicsLiveData.observeForever {
            if (it != null) {
                if (it.size == 20) {
                    TestApplication.mockWebServer!!.enqueueValidResponse(20,20)
                    viewModel.onLoadMoreRequested()
                }
                if (it.size == 40) {
                    countDownLatch.countDown()
                }
            }
        }
        countDownLatch.await()
    }
}