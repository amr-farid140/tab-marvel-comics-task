package me.amryousef.marvelcomic

import android.support.test.runner.AndroidJUnit4
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import me.amryousef.marvelcomic.storage.DataStore
import me.amryousef.marvelcomic.storage.PersistenceDatabase
import me.amryousef.marvelcomic.utility.TestApplication
import me.amryousef.marvelcomic.utility.enqueueValidResponse
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.standalone.inject
import org.koin.test.KoinTest
import java.util.concurrent.CountDownLatch

/**
 * Test that datastore is able to fetch and cache remote data
 */
@RunWith(AndroidJUnit4::class)
class DataStoreTest: KoinTest {
    private val dataStore:DataStore by inject()
    private val database: PersistenceDatabase by inject()
    private val mockWebServer = TestApplication.mockWebServer!!
    private var disposable: Disposable ? = null
    @Before
    fun setup() {
        disposable?.dispose()
        database.dao().clearComicListTable()
        mockWebServer.enqueueValidResponse()
        dataStore.getMoreRemoteData({
            throw it
        })
    }
    /**
     * Verifies that datastore is able to read cached data
     */
    @Test
    fun testDataIsCached() {
        val countDownLatch = CountDownLatch(1)
        disposable = dataStore.getCachedData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (!it.isEmpty() && it.size == 1) {
                        countDownLatch.countDown()
                    }
                }

        countDownLatch.await()
    }

    /**
     * Verifies that datastore is able to load and cache new data
     */
    @Test
    fun testCanCacheNewData(){
        val countDownLatch = CountDownLatch(1)
        mockWebServer.enqueueValidResponse(20,20)
        dataStore.getMoreRemoteData({
            throw it
        })
        disposable = dataStore.getCachedData()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (!it.isEmpty() && it.size == 2) {
                        countDownLatch.countDown()
                    }
                }

        countDownLatch.await()
    }


}