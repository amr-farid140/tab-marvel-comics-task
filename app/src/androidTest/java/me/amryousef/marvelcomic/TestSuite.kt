package me.amryousef.marvelcomic

import org.junit.runner.RunWith
import org.junit.runners.Suite

@RunWith(Suite::class)
@Suite.SuiteClasses(DataStoreTest::class,ComicsListViewModelTest::class)
class TestSuite