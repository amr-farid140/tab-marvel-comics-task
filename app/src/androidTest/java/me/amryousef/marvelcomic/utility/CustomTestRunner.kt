package me.amryousef.marvelcomic.utility

import android.app.Application
import android.app.Instrumentation
import android.content.Context
import android.support.test.runner.AndroidJUnitRunner

/**
 * Custom test runner for Instrumentation tests
 * Use TestApplication class to run application
 */
class CustomTestRunner: AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader?, className: String?, context: Context?): Application {
        return Instrumentation.newApplication(TestApplication::class.java,context)
    }
}