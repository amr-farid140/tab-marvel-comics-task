package me.amryousef.marvelcomic.utility

import io.reactivex.Flowable
import me.amryousef.marvelcomic.model.ComicList
import me.amryousef.marvelcomic.storage.DataStore
import me.amryousef.marvelcomic.storage.IDataStore

/**
 * MockDataStore class which allows mocking of IDataStore methods
 * or alternatively default to actual data store implementation
 */
class MockDataStore(private val actualStore:DataStore): IDataStore {
    var getCachedDataMock: () ->  Flowable<List<ComicList>>  = {
        actualStore.getCachedData()
    }
    var getMoreRemoteDataMock: (onError:(Throwable) -> Unit) -> Unit = {}

    override fun getCachedData(): Flowable<List<ComicList>> {
        return getCachedDataMock()
    }

    override fun getMoreRemoteData(onError: (Throwable) -> Unit) {
        getMoreRemoteDataMock(onError)
    }

}