package me.amryousef.marvelcomic.utility

import android.app.Application
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.mockwebserver.MockWebServer

/**
 * Test variant for Application
 */
class TestApplication: Application() {
    override fun onCreate() {
        mockWebServer = MockWebServer()
        //Avoids network on main thread exception by initializing MockWebServer on
        // computation thread then calling super.onCreate() after work is finished
        Observable.fromCallable {
            KoinMockModules.baseUrl = mockWebServer?.url("/").toString()
            KoinMockModules.build(this)
            super.onCreate()
        }.subscribeOn(Schedulers.computation()).observeOn(AndroidSchedulers.mainThread()).subscribe()
    }

    override fun onTerminate() {
        mockWebServer?.shutdown()
        super.onTerminate()
    }

    companion object {
        // Exposes mockWebServer instance to all the tests
        var mockWebServer: MockWebServer ? = null
    }
}