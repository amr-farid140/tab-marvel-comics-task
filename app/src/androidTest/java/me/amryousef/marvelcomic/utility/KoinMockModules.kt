package me.amryousef.marvelcomic.utility

import android.app.Application
import android.arch.persistence.room.Room

import com.google.gson.Gson
import me.amryousef.marvelcomic.R
import me.amryousef.marvelcomic.model.ComicList
import me.amryousef.marvelcomic.model.utility.ComicDeserializer
import me.amryousef.marvelcomic.network.ApiService
import me.amryousef.marvelcomic.network.RequestInterceptor
import me.amryousef.marvelcomic.presentation.viewModel.ComicsListViewModel
import me.amryousef.marvelcomic.storage.DataStore
import me.amryousef.marvelcomic.storage.IDataStore
import me.amryousef.marvelcomic.storage.PersistenceDatabase
import okhttp3.OkHttpClient
import org.koin.android.ext.android.startKoin
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.Module
import org.koin.dsl.module.applicationContext
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 *
 * Identical to KoinModule with the exception of some modules using Mocks or other testing variants
 *
 * @param context Application Context required for reading values from resources
 */
class KoinMockModules private constructor(private val context: Application) {

    private val okHttpClient: OkHttpClient get() {
        return OkHttpClient()
                .newBuilder()
                .connectTimeout(200, TimeUnit.SECONDS)
                .writeTimeout(200, TimeUnit.SECONDS)
                .readTimeout(200, TimeUnit.SECONDS)
                .addInterceptor(
                        RequestInterceptor(
                                context.getString(R.string.api_public_key),
                                context.getString(R.string.api_private_key)
                        )
                )
                .build()
    }
    private val gson:Gson get() {
        return Gson().newBuilder().registerTypeAdapter(ComicList::class.java,ComicDeserializer()).create()
    }
    private val apiService:ApiService get() {
        return Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build().create(ApiService::class.java)
    }

    private val gsonModule: Module = applicationContext {
        bean { gson }
    }
    private val apiServiceModule: Module = applicationContext {
        bean { apiService }
    }
    private val dataStoreModule: Module = applicationContext {
        bean { MockDataStore(get()) as IDataStore }
        bean { DataStore(get(),get()) }
    }
    private val databaseModule: Module = applicationContext {
        bean { Room.inMemoryDatabaseBuilder(androidApplication(),PersistenceDatabase::class.java).allowMainThreadQueries().build() }
    }
    private val comicsListModelModule = applicationContext {
        bean { ComicsListViewModel(get()) }
    }
    init {
        context.startKoin(listOf(databaseModule,gsonModule,apiServiceModule,dataStoreModule,comicsListModelModule))
    }

    companion object {
        var baseUrl = ""
        fun build(context: Application){
            if (baseUrl.isEmpty()) {
                baseUrl = context.getString(R.string.baseUrl)
            }
            KoinMockModules(context)
        }
    }
}